import 'package:flutter/material.dart';
import 'auth.dart';
import 'root_page.dart';

void main(){
  runApp(new MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "VendPKit",
      theme: new ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: RootPage(auth: new Auth())
    );
  }
}