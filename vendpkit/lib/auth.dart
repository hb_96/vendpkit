import 'package:firebase_auth/firebase_auth.dart';
import 'database_functions.dart';
import 'dart:async';

abstract class BaseAuth{
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String email, String password, String name);
  Future<String> currentUser();
  Future<void> signOut();
  Future<void> verifyEmail();
  Future<bool> isVerified();
}

class Auth implements BaseAuth{

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final Functions functions = Functions();

  Future<String> signInWithEmailAndPassword(String email, String password) async{
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  Future<String> createUserWithEmailAndPassword(String email, String password, String name) async{
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    UserUpdateInfo info = new UserUpdateInfo();
    info.displayName = name;
    _firebaseAuth.updateProfile(info);
    functions.create(user.uid, name, email);
    return user.uid;
  }

  Future<String> currentUser() async{
     FirebaseUser user = await _firebaseAuth.currentUser();
     return user.uid;
  }

  Future<void> signOut() async{
    return _firebaseAuth.signOut();
  }

  Future<void> verifyEmail() async{
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isVerified() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.reload();
    return user.isEmailVerified;
  }
}