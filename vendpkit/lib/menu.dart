import 'package:flutter/material.dart';
import 'auth.dart';
import 'database_functions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class MainMenu extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new _Menu();
}

class _Menu extends State<MainMenu>{
  final BaseAuth auth = Auth();
  final BaseFunctions functions = Functions();
  bool _busy = false;
  StreamSubscription<DocumentSnapshot> isbusy;
  final DocumentReference makeDrink = Firestore.instance.collection("rpi").document("makeDrink");

  initState(){
    super.initState();
    isbusy = makeDrink.snapshots().listen((dataSnapshot){
      if (dataSnapshot.exists) {
        setState(() {
          _busy = dataSnapshot.data['busy'];
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    isbusy?.cancel();
  }

  void _buy(int price, String name)async{
    String _uid;
    auth.currentUser().then((uid){
      _uid = uid;
      functions.getBalance(uid).then((_balance){
        if(_balance >= price){
          _check(price, name, _uid);
        } else{
          _error();
        }
      });
    });
  }

  Future<Null> _error() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Insufficient funds", style: new TextStyle(color: Colors.red),),
          content: new Text('Top up and try again'),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<Null> _check(int price, String drink, String uid) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Get a $drink", style: new TextStyle(color: Colors.brown),),
//          content: new Text('Let\'s get high on caffeine!'),
          actions: <Widget>[
            new RaisedButton(
              padding: new EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: Colors.brown,
              onPressed: () {Navigator.of(context).pop();},
              child: new Text("Nope"),
            ),
            new RaisedButton(
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: Color.fromARGB(255, 137, 211, 211),
              onPressed: (){
                functions.getDrink(drink, price);
                functions.changeBalance(uid, -price);
                functions.createReceipt(uid, drink, price);
                Navigator.of(context).pop();
              },
              child: new Text("Yup"),
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder(
      stream: Firestore.instance.collection('menu').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return new Stack(
          children: [
            new Opacity(
              opacity: 0.3,
              child: const ModalBarrier(dismissible: false, color: Colors.grey),
            ),
            new Center(
              child: new CircularProgressIndicator(),
            ),
          ],
        );
        return ListView.builder(
          itemCount: snapshot.data.documents.length,
          itemBuilder: (BuildContext context, int index){
            List<DocumentSnapshot> documents = snapshot.data.documents;
            documents.sort((a, b) => a.data['name'].compareTo(b.data['name']));
            String name = documents[index].data['name'];
            int price = documents[index].data['price'];
            return new Container (
              decoration: new BoxDecoration (
                  color: index%2==0? Color.fromARGB(120, 137, 211, 211):
                      Colors.white
              ),

              child: new ListTile (
                title: new Text('$name'),
                trailing: new Text('R$price'),
                onTap: (){_buy(price, name);},
                enabled: _busy == false,
              ),
            );

          },
        );
      }
    );
  }
}