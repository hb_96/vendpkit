import 'package:flutter/material.dart';
import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'auth.dart';
import 'dart:async';
import 'database_functions.dart';
import 'receipts.dart';
import 'menu.dart';

class HomePage extends StatefulWidget {
  HomePage({this.auth, this.onSignedOut});

  final BaseAuth auth;
  final VoidCallback onSignedOut;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

enum Verified{
  yes,
  no,
  waiting
}

class _HomePageState extends State<HomePage> {
  final BaseFunctions functions = Functions();
  String _userName;
  String _userEmail;
  String _uid;
  int _balance;

  StreamSubscription<DocumentSnapshot> balanceStream;

  final CollectionReference userCollection = Firestore.instance.collection("users");

  Verified _verified = Verified.waiting;

  initState() {
    super.initState();
    widget.auth.isVerified().then((verified) {
      setState(() {
        _verified = verified == false ? Verified.no : Verified.yes;
      });
    });

    widget.auth.currentUser().then((uid) {
      _uid = uid;

      userCollection.document(_uid).get().then((info) {
        if (info.exists) {
          setState(() {
            _userName = info.data['name'];
            _userEmail = info.data['email'];
          });
        }
      });

      balanceStream = userCollection.document(_uid).snapshots().listen((dataSnapshot) {
            if (dataSnapshot.exists) {
              setState(() {
                _balance = dataSnapshot.data['balance'];
                print('User id $_uid');
              });
            }
          });
    });
  }

  @override
  void dispose() {
    super.dispose();
    balanceStream?.cancel();
  }

  void _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    }
    catch (e) {
      print(e);
    }
  }

  void _addCredit() {
    functions.changeBalance(_uid, 5);
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Vendit"),
        centerTitle: true,
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: _userName == null ? new Text(
                  'loading...', style: new TextStyle(fontSize: 20.0, color: Colors.black)) :
              new Text(_userName, style: new TextStyle(fontSize: 20.0, color: Colors.black)),
              accountEmail: _userEmail == null ? new Text(
                  'loading...', style: new TextStyle(fontSize: 20.0, color: Colors.black)) :
              new Text(_userEmail, style: new TextStyle(fontSize: 20.0, color: Colors.black)),
              decoration: new BoxDecoration(
                color: Color.fromARGB(150, 137, 211, 211),
              ),
            ),
            new ListTile(
              title: _balance == null ? new Text(
                  'loading...', style: new TextStyle(fontSize: 20.0)) :
              new Text('R$_balance', style: new TextStyle(fontSize: 20.0)),
              leading: new Icon(Icons.account_balance_wallet),
            ),
            new Divider(),
            new ListTile(
              title: new Text('Credit Account'),
              leading: new Icon(Icons.credit_card),
              onTap: _addCredit,
              enabled: _verified == Verified.yes,
            ),
            new Divider(),
            new ListTile(
              title: new Text('Reciepts'),
              leading: new Icon(Icons.receipt),
              onTap: (){
                Navigator.of(context).pop();
                print('$_uid');
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new Receipts(_uid)));
              },
              enabled: _verified == Verified.yes,
            ),
            new Divider(),
            new ListTile(
              title: new Text('Share'),
              leading: new Icon(Icons.send),
              //onTap: ,
              enabled: false,
            ),
            new Divider(),
            new ListTile(
              title: new Text('Settings'),
              leading: new Icon(Icons.settings),
              // onTap: ,
              enabled: false,
            ),
            new Divider(),
            new ListTile(
              title: new Text('Log out'),
              leading: new Icon(Icons.account_circle),
              onTap: _signOut,
            ),
            new Divider(),
          ],
        ),
      ),
      body: MainMenu(),
    );
  }
}