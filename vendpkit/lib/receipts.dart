import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Receipts extends StatelessWidget{

  final String _uid;

  Receipts(this._uid);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Receipts'),
        centerTitle: true,
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection('users/$_uid/receipts').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return CircularProgressIndicator();
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext context, int index){
              List<DocumentSnapshot> documents = snapshot.data.documents;
              documents.sort((b, a) => a.data['dateTime'].compareTo(b.data['dateTime']));
              String name = documents[index].data['name'];
              String dateTime = documents[index].data['dateTime'].toString();
              int price = documents[index].data['price'];
              return new Container (
                decoration: new BoxDecoration (
                    color: index%2==0? Color.fromARGB(120, 137, 211, 211):
                    Colors.white
                ),

                child: new ListTile (
                  title: new Text('$name'),
                  subtitle: new Text('$dateTime'),
                  trailing: new Text('R$price'),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
