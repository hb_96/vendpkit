import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'auth.dart';
import 'password_field.dart';
import 'person_data.dart';

class LoginPage extends StatefulWidget{
  LoginPage({this.auth, this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  final _formKey = new GlobalKey<FormState>();
  final _passwordFieldKey = new GlobalKey<FormFieldState<String>>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  PersonData person = new PersonData();

  FormType _formType = FormType.login;


  bool _autoValidate = false;
  bool _loading = false;

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value)
    ));
  }

  String _validateName(String value) {
    if (value.isEmpty)
      return 'Name is required.';
    final RegExp nameExp = new RegExp(r'^[A-Za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String _validateEmail(String value) {
    if (value.isEmpty)
      return 'Email is required.';
    if (!value.contains('@')) {
      return 'Please enter valid email address.';
    }
    return null;
  }

  String _validatePassword(String value) {
    final FormFieldState<String> passwordField = _passwordFieldKey.currentState;
    if (passwordField.value == null || passwordField.value.isEmpty)
      return 'Please enter a password.';
    if (passwordField.value != value)
      return 'The passwords don\'t match';
    if(passwordField.value.length < 6)
      return 'Password is too short';
    return null;
  }

  void validateAndSubmit() async{
    final form = _formKey.currentState;
    if((!form.validate())){
      _autoValidate = true; // Start validating on every change.
      showInSnackBar('Please fix the errors in red before submitting.');
    }
    else{
      form.save();
      setState(() {
      _loading = true;
      });
      try{
        if(_formType == FormType.login){
          String userId = await widget.auth.signInWithEmailAndPassword(person.email, person.password);
          print('Signed in: $userId');
        } else{
          String userId = await widget.auth.createUserWithEmailAndPassword(person.email, person.password, person.name);
          await widget.auth.verifyEmail();
          print('Register user: $userId');
        }
        setState(() {
          _loading = false;
        });
        widget.onSignedIn();
      }
      catch(e){
        print('Error $e');
        _error(e.message);
        setState(() {
          _loading = false;
        });
      }
    }
  }

  void moveToRegister(){
    _formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin(){
    _formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }

  void _error(String error) {
    if(error == "A network error (such as timeout, interrupted connection or unreachable host) has occurred."){
      error = "No internet connection!";
    }
    else if(error == "There is no user record corresponding to this identifier. The user may have been deleted."){
      error = "Invalid email";
    }
    else if(error == "The password is invalid or the user does not have a password."){
      error = "Invalid password";
    }
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("ERROR", style: new TextStyle(color: Colors.red),),
          content: new Text(error),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    if(_loading == false){
      return new Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
            title: const Text('Vendit'),
            centerTitle: true,
          ),
          body: new SafeArea(
              top: false,
              bottom: false,
              child: new Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: new SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: buildInputs() + buildSubmitButtons(),
                    ),
                  )
              )
          )
      );
    }
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Vendit'),
        centerTitle: true,
      ),
      body: new Stack(
        children: [
          new SafeArea(
              top: false,
              bottom: false,
              child: new Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: new SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: buildInputs() + buildSubmitButtons(),
                    ),
                  )
              )
          ),
          new Opacity(
            opacity: 0.3,
            child: const ModalBarrier(dismissible: false, color: Colors.grey),
          ),
          new Center(
            child: new CircularProgressIndicator(),

          ),
        ],
      ),
    );
  }

  List<Widget> buildInputs(){
    if(_formType == FormType.login) {
      return [

        //Email
        const SizedBox(height: 24.0),
        new TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            filled: true,
            hintText: 'Your email address',
            labelText: 'E-mail',
          ),
          keyboardType: TextInputType.emailAddress,
          onSaved: (String value) { person.email = value; },
          validator: _validateEmail,
        ),

        //Password
        const SizedBox(height: 24.0),
        new PasswordField(
          fieldKey: _passwordFieldKey,
          labelText: 'Password',
          onSaved: (String value) { person.password = value; },
          validator: _validatePassword,
        ),
      ];
    } else{
      return[
        //Name
        const SizedBox(height: 24.0),
        new TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            filled: true,
            hintText: 'What do people call you?',
            labelText: 'Name',
          ),
          onSaved: (String value) { person.name = value; },
          validator: _validateName,
        ),

        //Email
        const SizedBox(height: 24.0),
        new TextFormField(
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            filled: true,
            hintText: 'Your email address',
            labelText: 'E-mail',
          ),
          keyboardType: TextInputType.emailAddress,
          onSaved: (String value) { person.email = value; },
          validator: _validateEmail,
        ),

        //Password
        const SizedBox(height: 24.0),
        new PasswordField(
          fieldKey: _passwordFieldKey,
          helperText: 'More than 6 characters',
          labelText: 'Password',
          onSaved: (String value) { person.password = value; },
          validator: _validatePassword,
        ),
        const SizedBox(height: 24.0),
        new TextFormField(
          //enabled: person.password != null && person.password.isNotEmpty,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            filled: true,
            labelText: 'Re-type password',
          ),
          obscureText: true,
          validator: _validatePassword,
        ),
      ];
    }
  }

  List<Widget> buildSubmitButtons(){
    if(_formType == FormType.login){
      return[
        new RaisedButton(
          child: new Text('Login', style: new TextStyle(fontSize: 20.0)),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text('Create an account', style: new TextStyle(fontSize: 20.0)),
          onPressed: moveToRegister,
        )
      ];
    } else{
      return[
        new RaisedButton(
          child: new Text('Create an account', style: new TextStyle(fontSize: 20.0)),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text('Have an account? Login', style: new TextStyle(fontSize: 20.0)),
          onPressed: moveToLogin,
        )
      ];
    }
  }
}

enum FormType{
  login,
  register
}