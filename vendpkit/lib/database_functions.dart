import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

abstract class BaseFunctions{
  void create(String uid, String name, String email);
  void changeBalance(String uid, int amount);
  Future<int> getBalance(String uid);
  void createReceipt(String uid, String name, int price);
  void getDrink(String name, int price);
}

class Functions extends BaseFunctions{
  final CollectionReference users = Firestore.instance.collection("users");
  final DocumentReference makeDrink = Firestore.instance.collection("rpi").document("makeDrink");
  
  void create(String uid, String name, String email){
    var userData = {
      'name': name,
      'email': email,
      'balance': 0,
    };

    var receiptData = {
      'dateTime': DateTime.now(),
      'name': 'Created Account',
      'price': 0,
    };
    users.document(uid).setData(userData).whenComplete((){
      print("User created!");
    }).catchError((e)=>print(e));
    users.document(uid).collection('receipts').document().setData(receiptData).whenComplete((){
      print("Receipts created");
    }).catchError((e)=>print(e));
  }

  void changeBalance(String uid, int amount){
    int balance;
    users.document(uid).get().then((dataSnapshot){
      if(dataSnapshot.exists){
        balance = dataSnapshot.data['balance'];
        balance = balance + amount;
        var data = {
          "balance": balance,
        };
        users.document(uid).updateData(data).whenComplete((){
          print("Balance changed");
        }).catchError((e)=>print(e));
      }
    }).catchError((e)=>print(e));
  }

  Future<int> getBalance(String uid) async{
    int balance;
    await users.document(uid).get().then((dataSnapshot){
      if(dataSnapshot.exists){
        balance = dataSnapshot.data['balance'];
      }
      print("got balance");
    }).catchError((e)=>print(e));
    return balance;
  }

  void createReceipt(String uid, String name, int price){
    var data = {
      'name': name,
      'price': price,
      'dateTime': DateTime.now(),
    };
    users.document(uid).collection('receipts').document().setData(data).whenComplete((){
      print("Receipt created!");
    }).catchError((e)=>print(e));
  }

  void getDrink(String name, int price){
    var data = {
      'busy': true,
      'drinkName': name,
      'price': price
    };
    makeDrink.setData(data).whenComplete((){
      print('Making a drink');
    }).catchError((e)=>print(e));
  }
}
